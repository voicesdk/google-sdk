# Google Home and Google Assistant SDK

_tbd_




## References

* [Google Assistant SDK](https://developers.google.com/assistant/sdk/)
* [Integrate the Assistant into Your Device](https://developers.google.com/assistant/sdk/develop/grpc/integrate)
* [embedded_assistant.proto](https://github.com/googleapis/googleapis/blob/master/google/assistant/embedded/v1alpha1/embedded_assistant.proto)
* [Google Assistant SDK for Python](https://github.com/googlesamples/assistant-sdk-python)


